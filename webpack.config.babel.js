import path from 'path';
import webpack from 'webpack';
import nodeExternals from 'webpack-node-externals';
import ExtractTextPlugin from "extract-text-webpack-plugin";

const common = {
  watch: true,
  module: {
    rules: [
      {
        test: /\.jsx?$/,
        loader: 'babel-loader',
        include: [path.resolve(__dirname, 'src')],
        query: {
          presets: [
            'env',
            'stage-2',
            'react'
          ]
        }
      },
      // {
      //   test: /\.sass$/,
      //   use: [ 'style-loader', 'css-loader', 'sass-loader' ]
      // },
      {
        test: /\.sass$/,
        use: ExtractTextPlugin.extract({ fallback: 'style-loader', use: ['css-loader', 'sass-loader'] })
      },
      { test: /\.(woff|woff2|eot|ttf|otf)$/, loader: 'file-loader' }
    ]
  }
};



const clientConfig = {
  ...common,

  name: 'client',
  target: 'web',

  entry: {
    client: [
      'babel-polyfill',
      './src/js/client.js'
    ]
  },
  output: {
    path: path.resolve(__dirname, 'build'),
    filename: '[name].js'
  },

  // TODO: use common chunks? Create new instance common plugins with ExtractTextPlugin
  // plugins: [
  //   new webpack.optimize.CommonsChunkPlugin({
  //     name: 'vendor',
  //     minChunks: module => /node_modules/.test(module.resource)
  //   }),
  // ],
  plugins: [
    new ExtractTextPlugin('main.css'),
    new webpack.DefinePlugin({
      'process.env.BROWSER': true,
    })
  ],

  devtool: 'cheap-module-source-map',

  node: {
    fs: 'empty',
    net: 'empty',
    tls: 'empty',
  }
};

const serverConfig = {
  ...common,

  name: 'server',
  target: 'node',
  externals: [nodeExternals()],

  entry: {
    server: ['babel-polyfill', path.resolve(__dirname, 'src', 'js', 'server.js')]
  },
  output: {
    path: path.resolve(__dirname, 'build'),
    filename: 'server.js'
  },

  plugins: [
    new ExtractTextPlugin('main.css'),
    new webpack.DefinePlugin({
      'process.env.BROWSER': false,
    })
  ],

  devtool: 'cheap-module-source-map',

  node: {
    console: true,
    global: false,
    process: false,
    Buffer: false,
    __filename: false,
    __dirname: false,
  }
}

export default [clientConfig, serverConfig];