import { createStore, applyMiddleware, compose } from 'redux';
import thunkMiddleware from 'redux-thunk';
import { createLogger } from 'redux-logger';
import todoApp from '../reducers/index';

const composeEnhancers = process.env.BROWSER ? (window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose) : compose;
const loggerMiddleware = createLogger();

// TODO: CLEAN THIS UP
let initialState = {}
if (process.env.BROWSER) {
  let user = JSON.parse(localStorage.getItem('user'));
  initialState = window.APP_STATE;

  if (user) {
    initialState = Object.assign({}, initialState, {
      authentication: {
        loggedIn: true,
        user: user
      }
    });
  }
}

export const store = process.env.BROWSER ? (
  createStore(
    todoApp,
    {...initialState},
    composeEnhancers(
      applyMiddleware(thunkMiddleware, loggerMiddleware)
    ))
) : (
  createStore(
    todoApp,
    composeEnhancers(
      applyMiddleware(thunkMiddleware, loggerMiddleware)
    )
));