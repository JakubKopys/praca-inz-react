export const taskService = {
  getAll,
  update,
  create,
  delete: deleteTask
};

function getAll(userID, token) {
  return fetch(`http://localhost:3000/v1/user/${userID}/tasks`, {
    method: "GET",
    headers: {
      'Content-Type': 'application/json',
      'Bearer': token
    },
    mode: 'cors'
  }).then(response => {
    if (!response.ok) {
      return Promise.reject({message: 'Error while getting tasks'});
    }
    return response.json();
  }).then(tasks => {
    return tasks;
  });
}

// TODO use user endpoint ?
function update(userID, token, task) {
  return fetch(`http://localhost:3000/v1/task/${task.id}`, {
    method: "PUT",
    headers: {
      'Content-Type': 'application/json',
      'Bearer': token
    },
    body: JSON.stringify({
      "title": task.title,
      "description": task.description,
      "state": task.state
    }),
    mode: 'cors'
  }).then(response => {
    if (!response.ok) {
      return Promise.reject({message: 'Error while updating task'});
    }
    return response.json();
  }).then(response => {
    return response;
  });
}

function deleteTask(token, taskId) {
  return fetch(`http://localhost:3000/v1/task/${taskId}`, {
    method: "DELETE",
    headers: {
      'Content-Type': 'application/json',
      'Bearer': token
    },
    mode: 'cors'
  }).then(response => {
    if (!response.ok) {
      return Promise.reject({message: 'Error while deleting task'});
    }
    return response.json();
  }).then(response => {
    return response;
  });
}

function create(userID, token, task) {
  return fetch(`http://localhost:3000/v1/user/${userID}/tasks`, {
    method: "POST",
    headers: {
      'Content-Type': 'application/json',
      'Bearer': token
    },
    body: JSON.stringify({
      "title": task.title,
      "description": task.description,
      "state": task.state
    }),
    mode: 'cors'
  }).then(response => {
    if (!response.ok) {
      return Promise.reject({message: 'Error while creating task'});
    }
    return response.json();
  }).then(response => {
    return response;
  });
}