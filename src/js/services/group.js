export const groupService = {
  getAll,
  create,
  delete: deleteGroup,
  getUsers,
  getTasks,
  createTask,
  updateTask,
  addUser,
  removeUser
};

function getAll(userID, token) {
  return fetch(`http://localhost:3000/v1/user/${userID}/groups`, {
    method: "GET",
    headers: {
      'Content-Type': 'application/json',
      'Bearer': token
    },
    mode: 'cors'
  }).then(response => {
    if (!response.ok) {
      return Promise.reject({message: 'Error while getting groups'});
    }
    return response.json();
  }).then(groups => {
    return groups;
  });
}

function create(token, group) {
  return fetch(`http://localhost:3000/v1/group`, {
    method: "POST",
    headers: {
      'Content-Type': 'application/json',
      'Bearer': token
    },
    body: JSON.stringify({
      "name": group.name
    }),
    mode: 'cors'
  }).then(response => {
    if (!response.ok) {
      return Promise.reject({message: 'Error while creating group'});
    }
    return response.json();
  }).then(message => {
    return message;
  });
}

function deleteGroup(token, groupId) {
  return fetch(`http://localhost:3000/v1/group/${groupId}`, {
    method: "DELETE",
    headers: {
      'Content-Type': 'application/json',
      'Bearer': token
    },
    mode: 'cors'
  }).then(response => {
    if (!response.ok) {
      return Promise.reject({message: 'Error while deleting group'});
    }
    return response.json();
  }).then(message => {
    return message;
  });
}

// ******************** TASKS ******************* //

function getTasks(token, groupId) {
  return fetch(`http://localhost:3000/v1/group/${groupId}/tasks`, {
    method: "GET",
    headers: {
      'Content-Type': 'application/json',
      'Bearer': token
    },
    mode: 'cors'
  }).then(response => {
    if (!response.ok) {
      return Promise.reject({message: 'Error while getting group\'s tasks'});
    }
    return response.json();
  }).then(tasks => {
    return tasks;
  });
}

function createTask(token, groupId, task) {
  return fetch(`http://localhost:3000/v1/group/${groupId}/tasks`, {
    method: "POST",
    headers: {
      'Content-Type': 'application/json',
      'Bearer': token
    },
    body: JSON.stringify({...task}),
    mode: 'cors'
  }).then(response => {
    if (!response.ok) {
      return Promise.reject({message: 'Error while creating group\'s task'});
    }
    return response.json();
  }).then(message => {
    return message;
  });
}

function updateTask(token, groupId, task) {
  return fetch(`http://localhost:3000/v1/task/${task.id}`, {
    method: "PUT",
    headers: {
      'Content-Type': 'application/json',
      'Bearer': token
    },
    body: JSON.stringify({  
      "title": task.title,
      "description": task.description,
      "state": task.state
    }),
    mode: 'cors'
  }).then(response => {
    if (!response.ok) {
      return Promise.reject({message: 'Error while updating group\'s task'});
    }
    return response.json();
  }).then(message => {
    return message;
  });
}

// ***************** USERS ********************* //

function getUsers(token, groupId) {
  return fetch(`http://localhost:3000/v1/group/${groupId}/users`, {
    method: "GET",
    headers: {
      'Content-Type': 'application/json',
      'Bearer': token
    },
    mode: 'cors'
  }).then(response => {
    if (!response.ok) {
      return Promise.reject({message: 'Error while getting group\'s users'});
    }
    return response.json();
  }).then(users => {
    return users;
  });
}

function addUser(token, groupId, userEmail) {
  return fetch(`http://localhost:3000/v1/group/${groupId}/users?email=${userEmail}`, {
    method: "POST",
    headers: {
      'Content-Type': 'application/json',
      'Bearer': token
    },
    mode: 'cors'
  }).then(response => {
    if (!response.ok) {
      return Promise.reject({message: 'Error while adding user'});
    }
    return response.json();
  }).then(message => {
    return message;
  });
}

function removeUser(token, groupId, userId) {
  return fetch(`http://localhost:3000/v1/group/${groupId}/users/${userId}`, {
    method: "DELETE",
    headers: {
      'Content-Type': 'application/json',
      'Bearer': token
    },
    mode: 'cors'
  }).then(response => {
    if (!response.ok) {
      return Promise.reject({message: 'Error while removing user'});
    }
    return response.json();
  }).then(message => {
    return message;
  });
}