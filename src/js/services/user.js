import { authHeader } from '../helpers';

function login(email, password) {
  return fetch(`http://localhost:3000/v1/signin`, {
    method: "POST",
    headers: {
      'Content-Type': 'application/json'
    },
    body: JSON.stringify({
      "email": email,
      "password": password,
    })
  }).then(response => {
    if (!response.ok) {
      return Promise.reject('Login failed, please try again');
    }
    return response.json();
  }).then(user => {
    // store user detauls and jwt token in local stroage to keep user loggged in
    localStorage.setItem('user', JSON.stringify(user));

    return user;
  });
}

function logout() {
  // remove user from local storage to log user out
  localStorage.removeItem('user');
}

function register(user) {
  return fetch(`http://localhost:3000/v1/signup`, {
    method: "POST",
    headers: {
      'Content-Type': 'application/json'
    },
    body: JSON.stringify({
      "username": user.username,
      "email": user.email,
      "password": user.password,
    })
  }).then(response => {
    // TODO:
    // 400 for failed validation -> need to get json from response in then
    if (!response.ok) { //  && response.status != 400
      return Promise.reject("Unexpected Error");
    }

    return response.json();
  }).then(response => {
    // TODO: add "errors" key when validation failed, check for it there
    return response;
  });
}


export const userService = {
  login,
  logout,
  register
};