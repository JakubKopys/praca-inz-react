import { hydrate } from 'react-dom';
import React, { Component } from 'react';
import { Provider  } from 'react-redux';

import { store } from './helpers';

import { App } from './components/App';

import { MuiThemeProvider, createMuiTheme } from 'material-ui/styles';
import { lightBlue, cyan } from 'material-ui/colors';

// Create a theme instance.
const theme = createMuiTheme({
  palette: {
    primary: lightBlue,
    secondary: cyan, 
    type: 'light',
  },
});

hydrate(
  <Provider store={store}>
    <MuiThemeProvider theme={theme}>
      <App/>
    </MuiThemeProvider>
  </Provider>,
  document.getElementById('app')
);