import { userConstants } from '../constants';
import { userService } from '../services';
import { alertActions } from './';
import { history } from '../helpers';

export const userActions = {
  login,
  register,
  logout
}

function login(email, password) {
  return dispatch => {
    dispatch(request(email));

    userService.login(email, password)
      .then(
        user => {
          dispatch(success(user));
          history.push('/');
        },
        error => {
          dispatch(failure());
          dispatch(alertActions.error(error));
        }
   );
  }

  // TODO: check what is returned, (probably You want to change it to token + user object)
  function request(email) { return { type: userConstants.LOGIN_REQUEST, email } }
  function success(user)  { return { type: userConstants.LOGIN_SUCCESS, user } }
  function failure()      { return { type: userConstants.LOGIN_FAILURE } }
}

function logout() {
  userService.logout();
  return { type: userConstants.LOGOUT };
}

function register(user) {
  return dispatch => {
    dispatch(request(user));

    userService.register(user).then(
      () => {
        dispatch(success());
        history.push('/login');
        dispatch(alertActions.success('Registration successful - you can login now'));
      },
      error => {
        dispatch(failure(error));
        dispatch(alertActions.error(error));
      }
    );
  }

  function request(user)  { return { type: userConstants.REGISTER_REQUEST, user } }
  function success()      { return { type: userConstants.REGISTER_SUCCESS } }
  function failure(error) { return { type: userConstants.REGISTER_FAILURE, error } }
}