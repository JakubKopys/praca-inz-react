import { taskConstants } from '../constants';
import { taskService } from '../services';

export const taskActions = {
  getAll,
  update,
  create,
  delete: deleteTask
}

// TODO CHANGE TO USERSTASK SERVICE

function getAll(userID, token) {
  return dispatch => {
    dispatch(request());

    taskService.getAll(userID, token)
      .then(
        tasks => {
          dispatch(success(tasks));
          dispatch({type: taskConstants.SELECT, taskId: tasks[0].Id}); // slect first task
        },
        error => dispatch(failure(error.message))
      );
  };

  function request()      { return { type: taskConstants.GETALL_REQUEST } }
  function success(tasks) { return { type: taskConstants.GETALL_SUCCESS, tasks } }
  function failure(error) { return { type: taskConstants.GETALL_FAILURE, error } }
}

// TODO update so I do not use userID (only task id)
// or just change to // TODO TO USERSTASK SERVICE
// TODO actually change only service
// TODO groups task will be listen on home!!
function update(userID, token, task) {
  return dispatch => {
    dispatch(request(task.id));

    taskService.update(userID, token, task)
      .then(
        () => dispatch(success(task)),
        error => dispatch(failure(error.message))
      );
  };

  function request(id)    { return { type: taskConstants.UPDATE_REQUEST, id } }
  function success(task)  { return { type: taskConstants.UPDATE_SUCCESS, task } }
  function failure(error) { return { type: taskConstants.UPDATE_FAILURE, error } }
}

function deleteTask(token, taskId) {
  return dispatch => {
    dispatch(request(taskId));

    taskService.delete(token, taskId)
      .then(
        () => dispatch(success(taskId)),
        error => dispatch(failure(error.message))
      );
  };

  function request(id)    { return { type: taskConstants.DELETE_REQUEST, id } }
  function success(id)    { return { type: taskConstants.DELETE_SUCCESS, id } }
  function failure(error) { return { type: taskConstants.DELETE_FAILURE, id: taskId, error } }
}

function create(userID, token, task) {
  return dispatch => {
    dispatch(request());

    taskService.create(userID, token, task)
      .then(
        () => {
          dispatch(success(task));
          dispatch(getAll(userID, token)); // get tasks from server
        },
        (error) => dispatch(failure(error.message))
      );
  };

  function request()      { return { type: taskConstants.CREATE_REQUEST } }
  function success(task)  { return { type: taskConstants.CREATE_SUCCESS, task } }
  function failure(error) { return { type: taskConstants.CREATE_FAILURE, error } }
}