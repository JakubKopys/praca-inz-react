import { groupConstants, groupUsersConstants, groupTasksConstants } from '../constants';
import { groupService, taskService } from '../services';

export const groupActions = {
  getAll,
  create,
  delete: deleteGroup,
  leave,
  select,
  getTasks,
  createTask,
  updateTask,
  deleteTask,
  getUsers,
  addUser,
  removeUser
}

function getAll(userID, token) {
  return dispatch => {
    dispatch(request());

    groupService.getAll(userID, token)
      .then(
        groups => {
          dispatch(success(groups));
          dispatch(select(token, groups[0].Id)); // slect first group
        },
        error => dispatch(failure(error.message))
      );
  };

  function request()       { return { type: groupConstants.GETALL_REQUEST } }
  function success(groups) { return { type: groupConstants.GETALL_SUCCESS, groups } }
  function failure(error)  { return { type: groupConstants.GETALL_FAILURE, error } }
}

function create(userID, token, group) {
  return dispatch => {
    dispatch(request());

    groupService.create(token, group)
      .then(
        () => {
          dispatch(success());
          dispatch(getAll(userID, token));
        },
        (error) => dispatch(failure(error.message))
      );
  };

  function request()      { return { type: groupConstants.CREATE_REQUEST } }
  function success()      { return { type: groupConstants.CREATE_SUCCESS } }
  function failure(error) { return { type: groupConstants.CREATE_FAILURE, error } }
}

function deleteGroup(token, groupId) {
  return dispatch => {
    dispatch(request());

    groupService.delete(token, groupId)
      .then(
        () => dispatch(success(groupId)),
        (error) => dispatch(failure(groupId, error.message))
      );
  };

  function request()               { return { type: groupConstants.DELETE_REQUEST } }
  function success(groupId)        { return { type: groupConstants.DELETE_SUCCESS, groupId } }
  function failure(groupId, error) { return { type: groupConstants.DELETE_FAILURE, groupId, error } }
}


function leave(token, groupId, userId) {
  return dispatch => {
    dispatch(request(groupId));

    groupService.removeUser(token, groupId, userId)
      .then(
        () => dispatch(success(groupId)),
        error => dispatch(failure(groupId, error.message))
      )
  }

  function request(groupId)        { return { type: groupConstants.LEAVE_REQUEST, groupId } }
  function success(groupId)        { return { type: groupConstants.LEAVE_SUCCESS, groupId } }
  function failure(groupId, error) { return { type: groupConstants.LEAVE_FAILURE, groupId, error } }
}

function select(token, groupId) {
  return dispatch => {
    dispatch({type: groupConstants.SELECT, groupId: groupId});
    dispatch(getUsers(token, groupId));
    dispatch(getTasks(token, groupId));
  }
}

// ************************************ USERS ********************************* //

function getUsers(token, groupId) {
  return dispatch => {
    dispatch(request());

    groupService.getUsers(token, groupId)
      .then(
        users => dispatch(success(users)),
        error => dispatch(failure(error.message))
      )
  }

  function request()      { return { type: groupUsersConstants.GETALL_REQUEST } }
  function success(users) { return { type: groupUsersConstants.GETALL_SUCCESS, users } }
  function failure(error) { return { type: groupUsersConstants.GETALL_FAILURE, error } }
}

function addUser(token, groupId, userEmail) {
  return dispatch => {
    dispatch(request());

    groupService.addUser(token, groupId, userEmail)
      .then(
        () => {
          dispatch(success());
          dispatch(getUsers(token, groupId)); // refresh user list
        },
        error => dispatch(failure(error.message))
      )
  }

  function request()      { return { type: groupUsersConstants.ADD_REQUEST } }
  function success()      { return { type: groupUsersConstants.ADD_SUCCESS } }
  function failure(error) { return { type: groupUsersConstants.ADD_FAILURE, error } }
}

function removeUser(token, groupId, userId) {
  return dispatch => {
    dispatch(request());

    groupService.removeUser(token, groupId, userId)
      .then(
        () => dispatch(success(userId)),
        error => dispatch(failure(error.message))
      )
  }

  function request()       { return { type: groupUsersConstants.REMOVE_REQUEST } }
  function success(userId) { return { type: groupUsersConstants.REMOVE_SUCCESS, userId } }
  function failure(error)  { return { type: groupUsersConstants.REMOVE_FAILURE, error } }
}

// ************************************************ TASKS *************************** //

function getTasks(token, groupId) {
  return dispatch => {
    dispatch(request());

    groupService.getTasks(token, groupId)
      .then(
        tasks => dispatch(success(tasks)),
        error => dispatch(failure(error.message))
      )
  }

  function request()      { return { type: groupTasksConstants.GETALL_REQUEST } }
  function success(tasks) { return { type: groupTasksConstants.GETALL_SUCCESS, tasks } }
  function failure(error) { return { type: groupTasksConstants.GETALL_FAILURE, error } }
}

function createTask(token, groupId, task) {
  return dispatch => {
    dispatch(request());

    groupService.createTask(token, groupId, task)
      .then(
        () => {
          dispatch(success());
          dispatch(getTasks(token, groupId));
        },
        error => dispatch(failure(error.message))
      )
  }

  function request()      { return { type: groupTasksConstants.CREATE_REQUEST } }
  function success()      { return { type: groupTasksConstants.CREATE_SUCCESS } }
  function failure(error) { return { type: groupTasksConstants.CREATE_FAILURE, error } }
}

function updateTask(token, groupId, task) {
  return dispatch => {
    dispatch(request(task.id));

    groupService.updateTask(token, groupId, task)
      .then(
        () => dispatch(success(task)),
        error => dispatch(failure(task.id, error.message))
      )
  }

  function request(taskId)        { return { type: groupTasksConstants.UPDATE_REQUEST, taskId } }
  function success(task)          { return { type: groupTasksConstants.UPDATE_SUCCESS, task } }
  function failure(taskId, error) { return { type: groupTasksConstants.UPDATE_FAILURE, taskId, error } }
}

function deleteTask(token, taskId) {
  return dispatch => {
    dispatch(request(taskId));

    taskService.delete(token, taskId)
      .then(
        () => {
          dispatch(success(taskId));
        },
        error => dispatch(failure(taskId, error.message))
      )
  }

  function request(taskId)        { return { type: groupTasksConstants.DELETE_REQUEST, taskId } }
  function success(taskId)        { return { type: groupTasksConstants.DELETE_SUCCESS, taskId } }
  function failure(taskId, error) { return { type: groupTasksConstants.DELETE_FAILURE, taskId, error } }
}