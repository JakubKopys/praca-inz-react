import { connect } from 'react-redux';

import { PrivateRoute } from '../components/PrivateRoute';

const mapStateToProps = state => {
  const { loggedIn } = state.authentication;
  return {
    loggedIn: loggedIn
  }
}

const ConnectedPrivateRoute = connect(mapStateToProps)(PrivateRoute);

export { ConnectedPrivateRoute as PrivateRoute };