import { connect } from 'react-redux';
import { Navbar } from '../components/Navbar';
import { userActions } from '../actions';

const mapStateToProps = state => {
  const { loggedIn } = state.authentication;
  return {
    loggedIn: loggedIn
  }
}

const mapDispatchToProps = dispatch => {
  return {
    signOut: () => {
      dispatch(userActions.logout())
    }
  }
}

const ConnectedNavbar = connect(
  mapStateToProps,
  mapDispatchToProps
)(Navbar)

export { ConnectedNavbar as Navbar };
