import React, { Component } from 'react';
import { ListItem, ListItemText, ListItemSecondaryAction } from 'material-ui/List';

const styles = { padding: '15px' };

// TODO: prop types, id, title as Required props
class TodoItem extends Component {
  handleItemClick = () => {
    const { id } = this.props;
    this.props.selectTask(id);
  }

  render() {
    const { id, title, description, state } = this.props;
    return (
      <ListItem style={styles} button onClick={this.handleItemClick}>
        <ListItemText primary={title} secondary={description} />
        <ListItemSecondaryAction>
          <span className={`task-${state} buffer-right`}>{state}</span>
        </ListItemSecondaryAction>
      </ListItem>
    )
  }
}

export { TodoItem };
