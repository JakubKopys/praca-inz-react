import React, { Component } from 'react';
import { connect } from 'react-redux';
import List from 'material-ui/List';
import { LinearProgress } from 'material-ui/Progress';
import Tabs, { Tab } from 'material-ui/Tabs';
import Grid from 'material-ui/Grid';

import { CenteredCard, GroupItem, Group, GroupModal, CreateGroupButton } from '.';
import { groupActions } from '../actions';

class Groups extends Component {
  constructor(props) {
    super(props);

    this.state = {
      modal: {
        opened: false,
        isEdit: false
      },
      currentGroup: {}
    };
  }

  componentDidMount() {
    const { id, token } = this.props.user;
    this.props.dispatch(groupActions.getAll(id, token));
  }

  componentWillReceiveProps(nextProps) {
    if (nextProps.updated) {
      this.setState({...this.state, modal: {...this.state.modal, opened: false}})
    }
  }

  closeModal = () => {
    this.setState({modal: {...this.state.modal, opened: false}});
  }

  setCurrentGroup = ({group, isEdit}) => {
    this.setState({currentGroup: {...group}, modal: {opened: true, isEdit: isEdit}});
  }

  selectGroup = (groupId) => {
    const { token } = this.props.user;
    this.props.dispatch(groupActions.select(token, groupId));
  }

  handleCreate = (group) => {
    const { id, token } = this.props.user;
    this.props.dispatch(groupActions.create(id, token, group));
  }

  handleDelete = (groupId) => {
    const { token } = this.props.user;
    this.props.dispatch(groupActions.delete(token, groupId));
  }

  handleLeave = (groupId) => {
    const { id, token } = this.props.user;
    this.props.dispatch(groupActions.leave(token, groupId, id));
  }

  render() {
    const { user, groups } = this.props;
    return (
      <Grid item xs={12}>
        <Grid container justify="center" spacing={40}>
          <CenteredCard>
            {groups.loading && <LinearProgress style={{margin: 20}} />}
            {groups.error && <span>ERROR: {groups.error}</span>}
            {groups.items &&
              <List>
                {groups.items.map((group, index) =>
                  <GroupItem
                    key={group.Id}
                    id={group.Id}
                    name={group.Name}
                    adminId={group.AdminID}
                    userId={user.id}
                    membersCount={group.MembersCount}
                    selectGroup={this.selectGroup}
                    handleDelete={this.handleDelete}
                    handleLeave={this.handleLeave}
                  />
                )}
              </List>
            }
          </CenteredCard>
          {groups.selectedGroup &&<Group group={groups.selectedGroup} />}
          <GroupModal
            open={this.state.modal.opened}
            onClose={this.closeModal}
            isEdit={this.state.modal.isEdit}
            group={this.state.currentGroup}
            handleCreate={this.handleCreate}
          />
          <CreateGroupButton setCurrentGroup={this.setCurrentGroup}/>
        </Grid>
      </Grid>
    );
  }
}

const mapStateToProps = state => {
  const { groups, authentication } = state;
  const { user } = authentication;
  return {
    user,
    groups
  }
}

const connectedGroups = connect(mapStateToProps)(Groups);
export { connectedGroups as Groups };