import React, { Component } from 'react';
import { connect } from 'react-redux';
import List from 'material-ui/List';
import { LinearProgress } from 'material-ui/Progress';
import Tabs, { Tab } from 'material-ui/Tabs';
import Grid from 'material-ui/Grid';

import { CenteredCard, Modal, TodoItem, Task, CreateTaskButton } from '.';
import { taskActions } from '../actions';
import { taskConstants } from '../constants/task';

class Home extends Component {
  constructor(props) {
    super(props);

    this.state = {
      modal: {
        opened: false,
        isEdit: false
      },
      filter: 'all',
      currentTask: {}
    };
  }
  componentDidMount() {
    const { id, token } = this.props.user;
    this.props.dispatch(taskActions.getAll(id, token));
  }

  componentWillReceiveProps(nextProps) {
    if (nextProps.updated) {
      this.setState({...this.state, modal: {...this.state.modal, opened: false}})
    }
  }

  closeModal = () => {
    this.setState({modal: {...this.state.modal, opened: false}});
  }

  setCurrentTask = ({task, isEdit}) => {
    this.setState({currentTask: {...task}, modal: {opened: true, isEdit: isEdit}});
  }

  handleTaskUpdate = (task) => {
    const { id, token } = this.props.user;
    this.props.dispatch(taskActions.update(id, token, task));
  }

  handleTaskCreation = (task) => {
    const { id, token } = this.props.user;
    this.props.dispatch(taskActions.create(id, token, task));
  }

  handleTabChange = (event, value) => {
    this.setState({...this.state, filter: value});
  }

  filterTasks = (tasks) => {
    return tasks.filter((task) => { return task.State == this.state.filter})
  }

  selectTask = (taskId) => {
    this.props.dispatch({type: taskConstants.SELECT, taskId: taskId});
  }

  deleteTask = (taskId) => {
    const { token } = this.props.user;
    this.props.dispatch(taskActions.delete(token, taskId));
  }

  render() {
    const { user, tasks } = this.props;
    const { filter } = this.state;

    const visibleTasks = filter == 'all' ? tasks.items : this.filterTasks(tasks.items);
    return (
      <Grid item xs={12}>
        <Grid container justify="center" spacing={40}>
          <CenteredCard>
            <Tabs
              value={this.state.filter}
              onChange={this.handleTabChange}
              indicatorColor="primary"
              textColor="primary"
              centered
            >
              <Tab label="ALL" value="all"/>
              <Tab label="TODO" value="todo"/>
              <Tab label="DOING" value="doing"/>
              <Tab label="DONE" value="done"/>
            </Tabs>
            {tasks.loading && <LinearProgress style={{margin: 20}} />}
            {tasks.error && <span>ERROR: {tasks.error}</span>}
            {visibleTasks &&
              <List>
                {visibleTasks.map((task, index) =>
                  <TodoItem
                    key={task.Id}
                    id={task.Id}
                    title={task.Title}
                    description={task.Description}
                    state={task.State}
                    selectTask={this.selectTask}
                  />
                )}
              </List>
            }
          </CenteredCard>
          <Modal
            open={this.state.modal.opened}
            onClose={this.closeModal}
            isEdit={this.state.modal.isEdit}
            id={this.state.currentTask.id}
            title={this.state.currentTask.title}
            description={this.state.currentTask.description}
            state={this.state.currentTask.state}
            handleTaskUpdate={this.handleTaskUpdate}
            handleTaskCreation={this.handleTaskCreation}
          />
          <Task
            loading={tasks.loading}
            error={tasks.error}
            task={tasks.selectedTask}
            setCurrentTask={this.setCurrentTask}
            deleteTask={this.deleteTask}
          />
          <CreateTaskButton setCurrentTask={this.setCurrentTask}/>
        </Grid>
      </Grid>
    );
  }
}

const mapStateToProps = state => {
  const { tasks, authentication } = state;
  const { user } = authentication;
  const { updated } = tasks;
  return {
    user,
    tasks,
    updated
  }
}

const connectedHome = connect(mapStateToProps)(Home);
export { connectedHome as Home };