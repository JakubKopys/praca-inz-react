import React, { Component } from 'react';
import Button from 'material-ui/Button';
import AddIcon from 'material-ui-icons/Add';
import Icon from 'material-ui/Icon';

const styles = {
  button: {
    position: 'absolute',
    bottom: '10%',
    right: '5%'
  }
}

// TODO one component (CreateTask/Group Button)
class CreateGroupButton extends Component {
  handleCreateClick = () => {
    const group = { title: '', description: '', state: '' }
    this.props.setCurrentGroup({group: group, isEdit: false});
  }

  render() {
    return (
      <Button
        fab
        onClick={this.handleCreateClick}
        color="secondary"
        style={styles.button}
      >
        <AddIcon />
      </Button>
    )
  }
}

export { CreateGroupButton };