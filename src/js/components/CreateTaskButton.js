import React, { Component } from 'react';
import Button from 'material-ui/Button';
import AddIcon from 'material-ui-icons/Add';
import Icon from 'material-ui/Icon';

const styles = {
  button: {
    position: 'absolute',
    bottom: '10%',
    right: '5%'
  }
}

class CreateTaskButton extends Component {
  handleCreateClick = () => {
    const task = { title: '', description: '', state: '' }
    this.props.setCurrentTask({task: task, isEdit: false});
  }

  render() {
    return (
      <Button
        fab
        onClick={this.handleCreateClick}
        color="secondary"
        style={styles.button}
      >
        <AddIcon />
      </Button>
    )
  }
}

export { CreateTaskButton };