import React, { Component } from 'react';
import { connect } from 'react-redux';
import { LinearProgress } from 'material-ui/Progress';
import Tabs, { Tab } from 'material-ui/Tabs';
import { CardHeader, CardContent } from 'material-ui/Card';


import { groupActions } from '../actions';
import { CenteredCard, GroupUsers, GroupTasks } from '.';

class Group extends Component {
  constructor(props) {
    super(props);

    this.state = {
      tab: "users"
    }
  }
  
  handleTabChange = (event, value) => {
    this.setState({...this.state, tab: value});
  }

  render() {
    const { group } = this.props;
    const { tab } = this.state;
    return (
      <CenteredCard>
        <CardHeader title={group.Name}/>
        <Tabs
          value={tab}
          onChange={this.handleTabChange}
          indicatorColor="primary"
          textColor="primary"
          centered
        >
          <Tab label="USERS" value="users"/>
          <Tab label="TASKS" value="tasks"/>
          <Tab label="CHAT" value="chat"/>
        </Tabs>
        {tab === "users" && <GroupUsers />}
        {tab === "tasks" && <GroupTasks />}
      </CenteredCard>
    )
  }
}

export { Group };