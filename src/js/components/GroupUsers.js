import React, { Component } from 'react';
import List, { ListItem, ListItemText, ListItemSecondaryAction } from 'material-ui/List';
import { connect } from 'react-redux';
import { LinearProgress } from 'material-ui/Progress';
import CardActions from 'material-ui/Card/CardActions';
import PersonAdd from 'material-ui-icons/PersonAdd';
import Clear from 'material-ui-icons/Clear';
import IconButton from 'material-ui/IconButton';
import TextField from 'material-ui/TextField';

import { groupActions } from '../actions';

const styles = { padding: '15px' };

class GroupUsers extends Component {
  constructor(props) {
    super(props);

    this.state = {
      adding: false,
      email: ''
    }
  }

  componentWillReceiveProps(nextProps) {
    this.setState({email: ''});
  }

  handleChange = (event) => {
    const { name, value } = event.target;
    this.setState({ [name]: value });
  }

  handleAddClick = () => {
    const { email } = this.state;
    const { token } = this.props.user;
    const groupId = this.props.group.Id;
    this.props.dispatch(groupActions.addUser(token, groupId, email));
  }

  handleRemoveClick = (event) => {
    const userId = event.currentTarget.dataset.userId;
    const { token } = this.props.user;
    const groupId = this.props.group.Id;
    this.props.dispatch(groupActions.removeUser(token, groupId, userId));
  }

  render() {
    const userId = Number(this.props.user.id);
    const adminId = this.props.group.AdminID;
    const { users } = this.props;
    const isAdmin = adminId === userId;

    return (
      <div>
        {users.loading && <LinearProgress style={{margin: 20}} />}
        {users.error && <span>ERROR: {users.error}</span>}
        <List>
          {users.items && users.items.map((user, index) =>
            <ListItem key={user.Id} style={styles} button>
              <ListItemText primary={user.Username} secondary={user.Email} />
              { user.Id === adminId &&
                <span className="buffer-right">admin</span>
              }
              { isAdmin && user.Id !== adminId &&
                <ListItemSecondaryAction data-user-id={user.Id} onClick={this.handleRemoveClick}>
                  <IconButton>
                    <Clear color="error"/>
                  </IconButton>
                </ListItemSecondaryAction>
              }
            </ListItem>
          )}
        </List>
        {
          isAdmin &&
          <CardActions>
            <TextField
              name="email"
              placeholder="User email"
              value={this.state.email}
              onChange={this.handleChange}
            />
            <IconButton onClick={this.handleAddClick}>
              <PersonAdd color="primary"/>
            </IconButton>
          </CardActions>
        }
      </div>
    )
  }
}

const mapStateToProps = (state) => {
  const { groups, authentication } = state;
  const { users } = groups;
  const { user } = authentication;
  return {
    users,
    user,
    group: groups.selectedGroup
  }
}

const ConnectedGroupUsers = connect(mapStateToProps)(GroupUsers);
export { ConnectedGroupUsers as GroupUsers };