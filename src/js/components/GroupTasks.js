import React, { Component } from 'react';
import List, { ListItem, ListItemText, ListItemSecondaryAction } from 'material-ui/List';
import { connect } from 'react-redux';
import { LinearProgress } from 'material-ui/Progress';
import CardActions from 'material-ui/Card/CardActions';
import Delete from 'material-ui-icons/Delete';
import Edit from 'material-ui-icons/Edit';
import Add from 'material-ui-icons/Add';
import Button from 'material-ui/Button';
import IconButton from 'material-ui/IconButton';

import { Modal } from './Modal';
import { groupActions } from '../actions';

const styles = { padding: '15px' };

class GroupTasks extends Component {
  state = {
    modal: {
      opened: false,
      task: { Title: '', Description: '', State: ''},
      isEdit: false
    }
  }

  openTaskInModal = ({task, isEdit}) => {
    this.setState({modal: {opened: true, isEdit: isEdit, task: {...task}}});
  }

  openEmptyTaskInModal = () => {
    this.setState({modal: {opened: true, isEdit: false, task: { Title: '', Description: '', State: ''}}});
  }
  
  handleEditClick = (event) => {
    const taskId = Number(event.currentTarget.dataset.taskId);
    const { tasks } = this.props;

    const task = tasks.items.find((task) => { return task.Id === taskId});
    this.openTaskInModal({task: task, isEdit: true});
  }

  handleDeleteClick = (event) => {
    const { token } = this.props.user;
    const taskId = Number(event.currentTarget.dataset.taskId);
    this.props.dispatch(groupActions.deleteTask(token, taskId));
  }

  isAdmin(task) {
    const { id } = this.props.user;
    const groupAdminId = this.props.group.AdminID;
    return groupAdminId === Number(id);
  }

  isOwner(task) {
    const { id } = this.props.user;
    return task.User && (task.User.Id === Number(id));
  }

  closeModal = () => {
    this.setState({modal: {...this.state.modal, opened: false}});
  }

  handleTaskUpdate = (task) => {
    const { token } = this.props.user;
    const groupId = this.props.group.Id;
    this.props.dispatch(groupActions.updateTask(token, groupId, task));
  }

  handleTaskCreation = (task) => {
    const { token } = this.props.user;
    const groupId = this.props.group.Id;
    this.props.dispatch(groupActions.createTask(token, groupId, task));
  }

  render() {
    const { tasks } = this.props;
    const { modal } = this.state;

    return (
      <div>
        {tasks.loading && <LinearProgress style={{margin: 20}} />}
        {tasks.error && <span>ERROR: {tasks.error}</span>}
        <List>
          {tasks.items && tasks.items.map((task, index) =>
            <ListItem key={task.Id} style={styles} button>
              <ListItemText primary={task.Title} secondary={task.Description} />
              { (this.isAdmin(task) || this.isOwner(task)) &&
                <ListItemSecondaryAction>
                  <IconButton data-task-id={task.Id} onClick={this.handleEditClick}>
                    <Edit color="primary"/>
                  </IconButton>
                  <IconButton data-task-id={task.Id} onClick={this.handleDeleteClick}>
                    <Delete color="error"/>
                  </IconButton>
                </ListItemSecondaryAction>
              }
            </ListItem>
          )}
        </List>
        <Modal
          open={modal.opened}
          onClose={this.closeModal}
          isEdit={modal.isEdit}
          id={modal.task.Id}
          title={modal.task.Title}
          description={modal.task.Description}
          state={modal.task.State}
          handleTaskUpdate={this.handleTaskUpdate}
          handleTaskCreation={this.handleTaskCreation}
        />
        <CardActions>
          <Button size="small" raised color="secondary" onClick={this.openEmptyTaskInModal}>
            Add Task
            <Add />
          </Button>
        </CardActions>
      </div>
    )
  }
}

const mapStateToProps = (state) => {
  const { groups, authentication } = state;
  const { tasks, selectedGroup } = groups;
  const { user } = authentication;
  return {
    tasks,
    user,
    group: selectedGroup
  }
}

const ConnectedGroupTasks = connect(mapStateToProps)(GroupTasks);
export { ConnectedGroupTasks as GroupTasks };