import React from 'react';
import PropTypes from 'prop-types';
import { sheetsRegistry } from 'react-jss/lib/ns';

class Html extends React.Component {
    static propTypes = {
      children: PropTypes.node.isRequired,
      initialState: PropTypes.object,
      scripts: PropTypes.array,
      MUIcss: PropTypes.any
    }

    render () {
        const { children, initialState, scripts, MUIcss } = this.props;
        
        return (
            <html>
            <head>
                <meta charSet="UTF-8" />
                <title>Server Side Rendered React App!!</title>
                {/* TODO: css passed from server.js */}
                <link rel="stylesheet" type="text/css" href="main.css"/>
            </head>
            <body>
                <div id="app"
                    dangerouslySetInnerHTML={{ __html: children }}
                ></div>
                {initialState && (
                    <script
                        dangerouslySetInnerHTML={{
                            __html: `window.APP_STATE=${JSON.stringify(initialState)}`
                        }}
                    ></script>
                )}
                {scripts.map((item, index) => {
                    return <script key={index} src={`${item}`}></script>;
                })}
                <style id="jss-server-side">${MUIcss}</style>
            </body>
            </html>
        );
    }
}

export default Html;