import React, { Component } from 'react';
import { FormLabel, FormControlLabel  } from 'material-ui/Form';
import TextField from 'material-ui/TextField';
import Button from 'material-ui/Button';
import { connect  } from 'react-redux';
import { userActions } from '../actions';

class Login extends Component {
  constructor(props) {
    super(props);
    // reset login status
    if (process.env.BROWSER) {
      this.props.dispatch(userActions.logout());
    }

    this.state = {
      email: '',
      password: ''
    };

    this.handleChange = this.handleChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
  }

  handleChange(event) {
    const { name, value } = event.target;
    this.setState({ [name]: value });
  }

  handleSubmit(event) {
    event.preventDefault();

    const { email, password } = this.state;

    if (email && password) {
      this.props.dispatch(userActions.login(email, password))
    }
  }

  render() {
    const { email, password } = this.state;
    const { loggingIn } = this.props;
    return (
      <form onSubmit={ this.handleSubmit }>
        <TextField
          name="email"
          placeholder="Email"
          margin="normal"
          fullWidth={true}
          onChange={this.handleChange}
        />
        <TextField
          name="password"
          placeholder="password"
          margin="normal"
          type="password"
          fullWidth={true}
          onChange={this.handleChange}
        />
        <Button raised color="secondary" type="submit" disabled={loggingIn}>
          Submit
        </Button>
      </form>
    );
  }
}

const mapStateToProps = state => {
  const { loggingIn } = state.authentication;
  return {
    loggingIn
  };
};

const connectedLogin = connect(mapStateToProps)(Login);
export { connectedLogin as Login };
