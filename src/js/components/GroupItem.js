import React, { Component } from 'react';
import { ListItem, ListItemText, ListItemSecondaryAction } from 'material-ui/List';
import Reply from 'material-ui-icons/Reply';
import Delete from 'material-ui-icons/Delete';
import IconButton from 'material-ui/IconButton';

const styles = { padding: '15px' };

// TODO: prop types, id, title as Required props
class GroupItem extends Component {
  handleClick = () => {
    const { id } = this.props;
    this.props.selectGroup(id);
  }

  handleDeleteClick = () => {
    const { id } = this.props;
    this.props.handleDelete(id);
  }

  handleLeaveClick = () => {
    const { id } = this.props;
    this.props.handleLeave(id);
  }

  render() {
    const { name, membersCount, adminId, userId } = this.props;
    const isAdmin = adminId === Number(userId);

    return (
      <ListItem style={styles} button onClick={this.handleClick}>
        <ListItemText primary={name} secondary={`Members: ${membersCount}`} />
        { isAdmin &&
          <ListItemSecondaryAction>
            <IconButton onClick={this.handleDeleteClick}>
              <Delete color="error"/>
            </IconButton>
          </ListItemSecondaryAction>
        }
        { !isAdmin &&
          <ListItemSecondaryAction>
            <IconButton onClick={this.handleLeaveClick}>
              <Reply />
            </IconButton>
          </ListItemSecondaryAction>
        }
      </ListItem>
    )
  }
}

export { GroupItem };