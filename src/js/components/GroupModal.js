import React, { Component } from 'react';
import Typography from 'material-ui/Typography';
import Modal from 'material-ui/Modal';
import Button from 'material-ui/Button';
import Paper from 'material-ui/Paper';
import TextField from 'material-ui/TextField';
import InputLabel from 'material-ui/Input/InputLabel';

const styles = {
  modal: {
    position: 'absolute',
    top: '40%',
    left: '50%',
    transform: "translate(-50%, -50%)"
  },
  paper: {
    padding: 15
  },
  form: {
    marginTop: 20
  },
  textField: {
    marginBottom: 20
  }
}

class GroupModal extends Component {
  constructor(props) {
    super(props);
    this.state = {name: ''};
  }

  componentWillReceiveProps(nextProps) {
    this.setState({name: nextProps.name});
  }

  handleChange = (event) => {
    const { name, value } = event.target;
    this.setState({ [name]: value });
  }

  handleSubmit = (event) => {
    event.preventDefault();

    const { name } = this.state;
    const { id, isEdit, handleCreate } = this.props;

    if (name) {
      const group = {name: name}
      handleCreate(group);
    }
  }

  render() {
    const { open, onClose, isEdit } = this.props;
    const header = isEdit ? "Edit Group" : "Create Group";
    return (
      <Modal
        aria-labelledby="simple-modal-title"
        aria-describedby="simple-modal-description"
        open={open}
        onClose={onClose}
      >
        <div style={styles.modal}>
          <Paper style={styles.paper}>
            <Typography type="title" id="modal-title">
              {header}
            </Typography>
            <form onSubmit={this.handleSubmit} style={styles.form}>
              <TextField
                label="Name"
                required
                name="name"
                margin="normal"
                fullWidth={true}
                value={this.state.title}
                onChange={this.handleChange}
              />
              <Button raised color="secondary" type="submit">
                Submit
              </Button>
            </form>
          </Paper>
        </div>
      </Modal>
    );
  }
}

export { GroupModal };
