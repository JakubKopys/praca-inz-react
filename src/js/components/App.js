import React, { Component } from 'react';
import { Router } from 'react-router-dom';
import { connect  } from 'react-redux';
import Reboot from 'material-ui/Reboot';

import { Navbar } from '../containers';
import { Main, Alert } from '.';
import { history } from '../helpers';
import { alertActions } from '../actions';

import '../../css/main.sass';

class App extends Component {
  constructor(props) {
    super(props);

    if (process.env.BROWSER) {
      history.listen((location, action) => {
        this.props.dispatch(alertActions.clear());
        // TODO: remove console.log
        console.log(action, location.pathname, location.state);
      });
    }
  }

  // Remove the server-side injected CSS.
  componentDidMount() {
    const jssStyles = document.getElementById('jss-server-side');
    if (jssStyles && jssStyles.parentNode) {
      jssStyles.parentNode.removeChild(jssStyles);
    }
  }

  render() {
    const { alert } = this.props;
    
    return(
      <Router history={history}>
        <div>
          <Reboot />
          <Navbar/>
          {alert.message && <Alert type={alert.type} message={alert.message}/>}
          <Main/>
        </div>
      </Router>
    )
  }
}

function mapStateToProps(state) {
  const { alert } = state;
  return {
      alert
  };
}

const connectedApp = connect(mapStateToProps)(App);
export { connectedApp as App };