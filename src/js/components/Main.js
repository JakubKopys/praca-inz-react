import React from 'react';
import { Switch, Route, Redirect, Link } from 'react-router-dom';
import Grid from 'material-ui/Grid';
import Paper from 'material-ui/Paper';

import { Login, Register, CenteredCard, Home, Groups } from '.';
import { PrivateRoute } from '../containers';

const Main = () => {
  // TODO w przyszłości mogę dodać router do tasków/taskid,
  // tylko coś nie pykało bo blokowało update czy coś idk
  // mógłbym w history.listen dispatchować akcję selectTask
  // i przy cofaniu (w history/listen?) ustawiać na poprzedni?
  // LOL tak, bo tam jest pop, ale to później
  return (
    <Grid container justify="center" style={{margin: '-30px'}}>
      <Switch>
        <PrivateRoute exact path='/' component={Home} />
        <PrivateRoute exact path='/groups' component={Groups} />
        <Route exact path='/login' component={LoginPage} />
        <Route exact path='/signup' component={RegisterPage} />
      </Switch>
    </Grid>
  )
}

const LoginPage = () => {
  return (
    <CenteredCard>
      <h2>Sign In</h2>
      <Login />
    </CenteredCard>
  )
}

const RegisterPage = () => {
  return (
    <CenteredCard>
      <h2>Sign Up</h2>
      <Register />
    </CenteredCard>
  )
}

export { Main };
