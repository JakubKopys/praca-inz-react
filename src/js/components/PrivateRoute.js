import { Route, Redirect } from 'react-router-dom';
import React, { Component } from 'react';

const PrivateRoute = ({ component: Component, loggedIn, ...rest }) => (
    <Route {...rest} render={props => (
      !process.env.BROWSER || loggedIn ? (
        <Component {...props}/>
      ) : (
        <Redirect to={{
          pathname: '/login',
          state: { from: props.location }
        }}/>
      )
    )}/>
  )

  export { PrivateRoute };