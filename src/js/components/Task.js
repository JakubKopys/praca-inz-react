import React, { Component } from 'react';
import { LinearProgress } from 'material-ui/Progress';
import Typography from 'material-ui/Typography';
import { CardHeader, CardMedia, CardContent, CardActions } from 'material-ui/Card';
import Button from 'material-ui/Button';
import IconButton from 'material-ui/IconButton';
import Edit from 'material-ui-icons/Edit';
import Delete from 'material-ui-icons/Delete';
import Avatar from 'material-ui/Avatar';

import { CenteredCard } from './CenteredCard';

class Task extends Component {

  handleEditClick = () => {
    const { task } = this.props;
    const currentTask = {
      id: task.Id,
      title: task.Title,
      description: task.Description,
      state: task.State
    };

    this.props.setCurrentTask({task: currentTask, isEdit: true});
  }

  handleDeleteClick = () => {
    const { Id } = this.props.task;
    this.props.deleteTask(Id);
  }

  render() {
    const { loading, error, task } = this.props;
    return (
      <CenteredCard>
        {loading && <LinearProgress style={{margin: 20}} />}
        {error && <span>ERROR: {error}</span>}
        {task &&
          <div>
            <CardHeader
              title={task.Title}
              subheader={<span className={`task-${task.State}`}>{task.State}</span>}
              action={
                <IconButton onClick={this.handleEditClick}>
                  <Edit color="primary"/>
                </IconButton>
              }
            />
            <CardContent>
              <Typography component="p">
                {task.Description}
              </Typography>
            </CardContent>
            <CardActions>
              <Button size="small" onClick={this.handleDeleteClick} raised color="secondary">
                Delete
                <Delete />
              </Button>
            </CardActions>
          </div>
        }
      </CenteredCard>
    );
  }
}

export { Task };