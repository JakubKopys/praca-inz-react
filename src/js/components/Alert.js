import React from 'react';
import { Paper, Grid } from 'material-ui';

const styles = {
  paper: {
    padding: 15,
    marginTop: 15,
    marginBottom: -70,
    color: 'white'
  }
}

const Alert = ({type, message}) => {
  return(
    <Grid container justify="center">
      <Grid item sm={4}>
        <Paper className={`alert ${type}`} style={styles.paper}>
          <div>{message}</div>
        </Paper>
      </Grid>
    </Grid>
  )
}

export { Alert };