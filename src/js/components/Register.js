import React, { Component } from 'react';
import { FormLabel, FormControlLabel  } from 'material-ui/Form'
import { TextField, Button } from 'material-ui'
import { connect  } from 'react-redux'

import { userActions } from '../actions'

class Register extends Component {
  constructor(props) {
    super(props);

    this.state = {
      user: {
        username: '',
        email: '',
        password: ''
      }
    };

    this.handleChange = this.handleChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
  }

  handleChange(event) {
    const { name, value } = event.target;
    const { user } = this.state;
    this.setState({
      user: {
        ...user,
        [name]: value
      }
    });
  }

  handleSubmit(event) {
    event.preventDefault();

    const { user } = this.state;
    const { dispatch } = this.props;

    if (user.username && user.email && user.password) {
      dispatch(userActions.register(user));
    }
  }

  render()  {
    const { registering } = this.props;
    const { user } = this.state;
    return (
      <form onSubmit={ this.handleSubmit }>
        <TextField
          name="username"
          placeholder="Username"
          margin="normal"
          fullWidth={true}
          onChange={this.handleChange}
        />
        <TextField
          name="email"
          placeholder="Email"
          margin="normal"
          fullWidth={true}
          onChange={this.handleChange}
        />
        <TextField
          name="password"
          placeholder="password"
          margin="normal"
          type="password"
          fullWidth={true}
          onChange={this.handleChange}
        />
        <Button raised color="secondary" type="submit" disabled={registering}>
          Sign Up
        </Button>
      </form>
    )
  }
}

const mapStateToProps = state => {
  const { registering } = state.registration;
  return {
    registering
  };
};

const connectedRegister = connect(mapStateToProps)(Register);
export { connectedRegister as Register };