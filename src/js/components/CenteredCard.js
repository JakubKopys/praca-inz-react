import React, { Component } from 'react';
import Grid from 'material-ui/Grid';
import Card from 'material-ui/Card';

const styles = {
  card: {
    padding: 15,
    marginTop: 75,
  }
}

class CenteredCard extends Component {
  render() {
    return (
      <Grid item lg={4} sm={10}>
        <Card style={styles.card}>
          {this.props.children}
        </Card>
      </Grid>
    )
  }
}

export { CenteredCard };
