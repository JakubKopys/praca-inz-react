import React, { Component } from 'react';
import AppBar from 'material-ui/AppBar';
import Button from 'material-ui/Button';
import Toolbar from 'material-ui/Toolbar';
import Link from 'react-router-dom/Link';

const styles = {
  appbar: {
    width: '100%',
    button: {
      textDecoration: 'none',
      text: {
        color: 'white',
      },
    },
  },
  flex: {
    flex: 1,
  },
}

class Navbar extends Component {
  render() {
    let links = null;

    // TODO change
    if (!process.env.BROWSER || this.props.loggedIn) {
      links = (
        <div>
          <ButtonLink to="/" text="Home" />
          <ButtonLink to="/groups" text="Groups" />
          <Button style={styles.appbar.button.text} onClick={this.props.signOut}>Log Out</Button>
        </div>
      )
    } else {
      links = (
        <div>
          <ButtonLink to="/login" text="Sign In" />
          <ButtonLink to="/signup" text="Sign Up" />
        </div>
      )
  }

  return (
    <AppBar position="static" style={styles.appbar}>
      <Toolbar>
        {links}
      </Toolbar>
    </AppBar>
    )
  }
}

const ButtonLink = (props) => {
  return (
    <Link style={styles.appbar.button} to={props.to}>
      <Button style={styles.appbar.button.text} >{props.text}</Button>
    </Link>
  )
}

export { Navbar };
