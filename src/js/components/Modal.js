import React, { Component } from 'react';
import Typography from 'material-ui/Typography';
import Modal from 'material-ui/Modal';
import Button from 'material-ui/Button';
import Paper from 'material-ui/Paper';
import TextField from 'material-ui/TextField';
import InputLabel from 'material-ui/Input/InputLabel';

// TODO: to helpers?
const states = [
  {
    value: '',
    label: 'none'
  },
  {
    value: 'todo',
    label: 'todo'
  },
  {
    value: 'doing',
    label: 'doing'
  },
  {
    value: 'done',
    label: 'done'
  }
]

const styles = {
  modal: {
    position: 'absolute',
    top: '40%',
    left: '50%',
    transform: "translate(-50%, -50%)"
  },
  paper: {
    padding: 15
  },
  form: {
    marginTop: 20
  },
  textField: {
    marginBottom: 20
  }
}

class EditModal extends Component {
  constructor(props) {
    super(props);

    this.state = {
      title: '',
      description: '',
      state: ''
    };
  }

  componentWillReceiveProps(nextProps) {
    this.setState({
      title: nextProps.title,
      description: nextProps.description,
      state: nextProps.state
    });
  }

  handleChange = (event) => {
    const { name, value } = event.target;
    this.setState({ [name]: value });
  }

  handleSubmit = (event) => {
    event.preventDefault();

    const { title, description, state } = this.state;
    const { id, isEdit, handleTaskUpdate, handleTaskCreation } = this.props;

    if (title && description) {
      const task = {id: id, title: title, description: description, state: state}
      isEdit ? handleTaskUpdate(task) : handleTaskCreation(task);
      this.props.onClose();
    }
  }

  render() {
    const { open, onClose, isEdit } = this.props;
    const header = isEdit ? "Edit Task" : "Add Task";
    return (
      <Modal
        aria-labelledby="simple-modal-title"
        aria-describedby="simple-modal-description"
        open={open}
        onClose={onClose}
      >
        <div style={styles.modal}>
          <Paper style={styles.paper}>
            <Typography type="title" id="modal-title">
              {header}
            </Typography>
            {/* TODO: to separate component */}
            <form onSubmit={this.handleSubmit} style={styles.form}>
              <TextField
                label="Title"
                required
                name="title"
                margin="normal"
                fullWidth={true}
                value={this.state.title}
                onChange={this.handleChange}
              />
              <TextField
                label="Description"
                required
                name="description"
                multiline
                rows="4"
                margin="normal"
                fullWidth
                value={this.state.description}
                onChange={this.handleChange}
                style={styles.textField}
              />
              <InputLabel required>State</InputLabel>
              <TextField
                select
                name="state"
                required
                value={this.state.state}
                onChange={this.handleChange}
                style={{marginTop: 0, marginBottom: '15px'}}
                fullWidth
                margin="normal"
                SelectProps={{native: true}}
              >
                {states.map(option => (
                  <option key={option.value} value={option.value}>
                    {option.label}
                  </option>
                ))}
              </TextField>
              <Button raised color="secondary" type="submit">
                Submit
              </Button>
            </form>
          </Paper>
        </div>
      </Modal>
    );
  }
}

export { EditModal as Modal };
