import express from 'express';
import path from 'path';
import React from 'react';
import ReactDOMServer from 'react-dom/server';

import { Provider  } from 'react-redux';
import { store  } from './helpers';

import { StaticRouter } from 'react-router';

import Html from './components/Html';
import { App } from './components/App';

import { SheetsRegistry } from 'react-jss/lib/jss';
import JssProvider from 'react-jss/lib/JssProvider';
import { MuiThemeProvider, createMuiTheme, createGenerateClassName } from 'material-ui/styles';
import { lightBlue, cyan } from 'material-ui/colors';


const app = express();

app.use(express.static(path.join(__dirname)));

// TODO try going back to app.get('*', async (req, res, next) =>)
app.use((req, res) => {
    // Create a sheetsRegistry instance.
    const sheetsRegistry = new SheetsRegistry();

    // Create a theme instance.
    const theme = createMuiTheme({
      palette: {
        primary: lightBlue,
        accent: cyan,
        type: 'light',
      },
    });

    const generateClassName = createGenerateClassName();

    const scripts = ['client.js'];
    // TODO: same as scripts - add CSS files (main css)
    const styles = ['main.css']
    // TODO: LEGIT initial state (probably empty stuff lol)
    const initialState = { authentication: {}, registration: {}, alert: {} };
    const context = {};

    const appMarkup = ReactDOMServer.renderToString(
        <StaticRouter location={req.url} context={context}>
            <Provider store={store}>
                <JssProvider registry={sheetsRegistry} generateClassName={generateClassName}>
                    <MuiThemeProvider theme={theme} sheetsManager={new Map()}>
                        <App {...initialState} />
                    </MuiThemeProvider>
                </JssProvider>
            </Provider>
        </StaticRouter>
    );

    // Grab the CSS from our sheetsRegistry.
    const css = sheetsRegistry.toString()

    const html = ReactDOMServer.renderToStaticMarkup(
        <Html children={appMarkup}
              scripts={scripts}
              initialState={initialState}
              MUIcss={css} />
    );

    // if (context.url) {
    //   // Somewhere a `<Redirect>` was rendered
    //   res.redirect(301, context.url);
    // } else {
    //   // we're good, send the response
    //   res.send(`<!doctype html>${html}`);
    // }
    res.send(`<!doctype html>${html}`);

});

app.listen(2137, () => console.log('Listening on localhost:2137'));
