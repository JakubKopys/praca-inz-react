import { userConstants } from '../constants';

let user = process.env.BROWSER ? JSON.parse(localStorage.getItem('user')) : null;
const initialState = user ? { loggedIn: true, user } : {};

export const authentication = (state = initialState, action) => {
  switch (action.type) {
    case userConstants.LOGIN_REQUEST:
      return Object.assign({}, state, {
        loggingIn: true,
        user: {
          email: action.email
        }
      });
    case userConstants.LOGIN_SUCCESS:
      return Object.assign({}, state, {
        loggedIn: true,
        loggingIn: false,
        user: action.user
      });
    case userConstants.LOGIN_FAILURE:
      return {};
    case userConstants.LOGOUT:
      return {};
    default:
      return state;
  }
}