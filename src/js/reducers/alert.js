import { alertConstants } from '../constants';

export const alert = (state = {}, action) => {
	switch (action.type) {
		case alertConstants.SUCCESS:
      return Object.assign({}, state, {
        type: 'alert-success',
        message: action.message
      });
    case alertConstants.ERROR:
      return Object.assign({}, state, {
        type: 'alert-danger',
        message: action.message
      });
    case alertConstants.CLEAR:
      return {};
    default:
      return state;
	}
}