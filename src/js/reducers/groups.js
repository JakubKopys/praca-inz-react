import { groupConstants, groupUsersConstants, groupTasksConstants } from '../constants';

const groups = (state = {items: [], users: {}, tasks: {}}, action) => {
  switch (action.type) {
    case groupConstants.GETALL_REQUEST:
      return {
        ...state,
        loading: true
      };
    case groupConstants.GETALL_SUCCESS:
      return {
        items: action.groups,
        users: state.users,
        tasks: state.tasks
      };
    case groupConstants.GETALL_FAILURE:
      return {
        error: action.error
      };
    case groupConstants.SELECT:
      return {
        ...state,
        selectedGroup: state.items.find((group) => { return group.Id == action.groupId })
      };
    case groupConstants.DELETE_REQUEST:
      return {
        ...state,
        items: state.items.map(group =>
          group.Id === action.id
            ? { ...group, deleting: true }
            : group
        )
      };
    case groupConstants.DELETE_SUCCESS:
      const filteredItems = state.items.filter(item => item.Id !== action.groupId);
      const newSelected = state.selectedGroup.Id === action.groupId ? (
        filteredItems[0]
      ) : state.selectedGroup;

      return {
        ...state,
        items: filteredItems,
        selectedGroup: newSelected
      };
    case groupConstants.DELETE_FAILURE:
      return {
        ...state,
        items: state.items.map(group => {
          if (group.Id === action.groupId) {
            const { deleting, ...groupCopy } = group;
            return { ...groupCopy, deleteError: action.error };
          }
          return group;
        }),
      };
    case groupConstants.LEAVE_REQUEST:
      return {
        ...state,
        items: state.items.map(group =>
          group.Id === Number(action.groupId)
            ? { ...group, leaving: true }
            : group
        )
      };
    case groupConstants.LEAVE_SUCCESS:
      return {
        ...state,
        items: state.items.filter((group) => {return group.Id !== Number(action.groupId)})
      }
    case groupConstants.LEAVE_FAILURE:
      return {
        ...state,
        items: state.items.map(group => {
          if (group.Id === Number(action.groupId)) {
            const { leaving, ...groupCopy } = group;
            return { ...groupCopy, leaveError: action.error };
          }
          return group;
        }),
      };
    // Nested reducer for groups: { tasks: {...} }
    case groupTasksConstants.GETALL_REQUEST:
    case groupTasksConstants.GETALL_SUCCESS:
    case groupTasksConstants.GETALL_FAILURE:
    case groupTasksConstants.CREATE_REQUEST:
    case groupTasksConstants.CREATE_SUCCESS:
    case groupTasksConstants.CREATE_FAILURE:
    case groupTasksConstants.UPDATE_REQUEST:
    case groupTasksConstants.UPDATE_SUCCESS:
    case groupTasksConstants.UPDATE_FAILURE:
    case groupTasksConstants.DELETE_REQUEST:
    case groupTasksConstants.DELETE_SUCCESS:
    case groupTasksConstants.DELETE_FAILURE:
      return {...state, tasks: groupTasks(state.tasks, action)}
    // Nested reducer for groups: { users: {...} }
    case groupUsersConstants.GETALL_REQUEST:
    case groupUsersConstants.GETALL_SUCCESS:
    case groupUsersConstants.GETALL_FAILURE:
    case groupUsersConstants.ADD_REQUEST:
    case groupUsersConstants.ADD_SUCCESS:
    case groupUsersConstants.ADD_FAILURE:
    case groupUsersConstants.REMOVE_REQUEST:
    case groupUsersConstants.REMOVE_SUCCESS:
    case groupUsersConstants.REMOVE_FAILURE:
      return {...state, users: groupUsers(state.users, action)}
    default:
      return state;
  }
}

const groupUsers = (state = {}, action) => {
  switch (action.type) {
    case groupUsersConstants.GETALL_REQUEST:
      return {
        loading: true
      }
    case groupUsersConstants.GETALL_SUCCESS:
      return {
        items: action.users
      }
    case groupUsersConstants.GETALL_FAILURE:
      return {
        error: action.error
      }
    case groupUsersConstants.ADD_REQUEST:
      return {
        ...state,
        adding: true
      }
    case groupUsersConstants.ADD_SUCCESS:
      return state;
    case groupUsersConstants.ADD_FAILURE:
      return {
        error: action.error
      }
    case groupUsersConstants.REMOVE_REQUEST:
      return {
        ...state,
        removing: true
      }
    case groupUsersConstants.REMOVE_SUCCESS:
      return {
        ...state,
        items: state.items.filter((user) => {return user.Id !== Number(action.userId)})
      }
    case groupUsersConstants.REMOVE_FAILURE:
      return {
        error: action.error
      }
    default:
      return state;
  }
}

const groupTasks = (state = {}, action) => {
  switch(action.type) {
    case groupTasksConstants.GETALL_REQUEST:
      return {
        loading: true
      }
    case groupTasksConstants.GETALL_SUCCESS:
      return {
        items: action.tasks
      }
    case groupTasksConstants.GETALL_FAILURE:
      return {
        error: action.error
      }
    case groupTasksConstants.CREATE_REQUEST:
      return {
        ...state,
        adding: true
      }
    case groupTasksConstants.CREATE_SUCCESS:
      return state;
    case groupTasksConstants.CREATE_FAILURE:
      return {
        error: action.error
      }
    case groupTasksConstants.UPDATE_REQUEST:
      return {
        ...state,
        items: state.items.map(task =>
          task.Id === action.id
            ? { ...task, updating: true }
            : task
        )
      };
    case groupTasksConstants.UPDATE_SUCCESS:
      return {
        ...state,
        items: state.items.map(task =>
          task.Id === action.task.id
            ? {...task, State: action.task.state, Title: action.task.title, Description: action.task.description}
            : task
        ),
        updated: true
      };
    case groupTasksConstants.UPDATE_FAILURE:
      // TODO assign updateError to specific task?
      return {
        ...state,
        error: action.error
      };
    case groupTasksConstants.DELETE_REQUEST:
      return {
        ...state,
        items: state.items.map(task =>
          task.Id === action.taskId
            ? { ...task, deleting: true }
            : task
        )
      };
    case groupTasksConstants.DELETE_SUCCESS:
      const filteredItems = state.items.filter(item => item.Id !== action.taskId);
      return {
        ...state,
        items: filteredItems
      };
    case groupTasksConstants.DELETE_FAILURE:
      return {
        ...state,
        items: state.items.map(task => {
          if (task.Id === action.taskId) {
            const { deleting, ...taskCopy } = task;
            return { ...taskCopy, deleteError: action.error };
          }
          return task;
        }),
      };
    default:
      return state;
  }
}

export { groups };