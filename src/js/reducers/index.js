import { combineReducers  } from 'redux';

import { registration } from './registration';
import { authentication } from './authentication';
import { alert } from './alert';
import { tasks } from './tasks';
import { groups } from './groups';

const reducers = {
  authentication,
  registration,
  alert,
  tasks,
  groups
}

const todoApp = combineReducers(reducers)

export default todoApp
