import { taskConstants } from '../constants';

export const tasks = (state = {}, action) => {
  switch (action.type) {
    case taskConstants.GETALL_REQUEST:
      return {
        loading: true
      };
    case taskConstants.GETALL_SUCCESS:
      return {
        items: action.tasks
      };
    case taskConstants.GETALL_FAILURE:
      return {
        error: action.error
      };
    case taskConstants.CREATE_REQUEST:
      return {
        ...state,
        creating: true
      };
    case taskConstants.CREATE_SUCCESS:
      return state;
    case taskConstants.CREATE_FAILURE:
      return {
        ...state,
        error: action.error
      }
    case taskConstants.UPDATE_REQUEST:
      return {
        ...state,
        items: state.items.map(task =>
          task.Id === action.id
            ? { ...task, updating: true }
            : task
        )
      };
    case taskConstants.UPDATE_SUCCESS:
      return {
        ...state,
        items: state.items.map(task =>
          task.Id === action.task.id
            ? {...task, State: action.task.state, Title: action.task.title, Description: action.task.description}
            : task
        ),
        selectedTask: action.task.id === state.selectedTask.Id ? (
          {...state.selectedTask, State: action.task.state, Title: action.task.title, Description: action.task.description}
        ) : (
          state.selectedTask
        ),
        updated: true
      };
    case taskConstants.UPDATE_FAILURE:
      return {
        ...state,
        error: action.error
      };
    case taskConstants.DELETE_REQUEST:
      return {
        ...state,
        items: state.items.map(task =>
          task.Id === action.id
            ? { ...task, deleting: true }
            : task
        )
      };
    case taskConstants.DELETE_SUCCESS:
      const filteredItems = state.items.filter(task => task.Id !== action.id);
      return {
        items: filteredItems,
        selectedTask: filteredItems[0],
      };
    case taskConstants.DELETE_FAILURE:
      return {
        ...state,
        items: state.items.map(task => {
          if (task.Id === action.id) {
            // make copy of task without 'deleting:true' property
            const { deleting, ...taskCopy } = task;
            // return copy of task with 'deleteError:[error]' property
            return { ...taskCopy, deleteError: action.error };
          }
          return task;
        })
      };
    case taskConstants.SELECT:
      return {
        ...state,
        selectedTask: state.items.find((task) => { return task.Id == action.taskId })
      }
    default:
      return state;
  }
}